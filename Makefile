up: docker-up
init: docker-down docker-pull docker-build docker-up apple-init
test: apple-test

apple-init: apple-composer-install apple-migrations apple-fixtures

apple-migrations:
	docker-compose run --rm apple-php-cli php bin/console doctrine:migrations:migrate --no-interaction

apple-fixtures:
	docker-compose run --rm apple-php-cli php bin/console doctrine:fixtures:load --no-interaction

apple-composer-install:
	docker-compose run --rm apple-php-cli composer install

apple-test:
	docker-compose run --rm apple-php-cli php bin/phpunit

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-build:
	docker-compose build

docker-pull:
	docker-compose pull

