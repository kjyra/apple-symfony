<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Model\Apple\Entity\Apple\Apple;
use App\Model\Apple\Entity\Apple\Id;
use App\Model\Apple\Entity\Apple\Status;
use App\Model\Apple\Entity\Apple\Size;

class AppleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $apple = new Apple(Id::next(), 'red', new \DateTimeImmutable(), new Status(Status::getOnTreeStatus()), new Size(1.00));
        $apple2 = new Apple(Id::next(), 'green', new \DateTimeImmutable(), new Status(Status::getOnTreeStatus()), new Size(1.00));
		
		$manager->persist($apple);
		$manager->persist($apple2);
		
        $manager->flush();
    }
}
