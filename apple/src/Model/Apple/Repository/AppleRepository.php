<?php

namespace App\Model\Apple\Repository;

use App\Model\Apple\Entity\Apple\Apple;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\EntityNotFoundException;
use App\Model\Apple\Entity\Apple\Id;

/**
 * Description of AppleRepository
 *
 * @author kjyra
 */
class AppleRepository implements AppleRepositoryInterface
{
	
	private $em;
	private $repo;
	
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(Apple::class);
	}
	
	public function add(Apple $apple): void
	{
		$this->em->persist($apple);
	}
	
	public function getAll(): array
	{
		if(!$apples = $this->repo->findAll()) {
			throw new EntityNotFoundException('Apples are not found');
		}
		return $apples;
	}
	
	public function getById(Id $id): Apple
	{
		if(!$apple = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Apple is not found');
        }
        return $apple;
	}
	
	public function delete(Apple $apple): void
	{
		$this->em->remove($apple);
	}
	
	public function deleteAll(array $apples): void
	{
		foreach($apples as $apple) {
			$this->delete($apple);
		}
	}

}
