<?php

namespace App\Model\Apple\Repository;

use App\Model\Apple\Entity\Apple\Apple;
use App\Model\Apple\Entity\Apple\Id;

/**
 *
 * @author kjyra
 */
interface AppleRepositoryInterface
{
	
	public function add(Apple $apple): void;
	
	public function getAll(): array;
	
	public function getById(Id $id): Apple;
	
	public function delete(Apple $apple);
	
	public function deleteAll(array $apples): void;
}
