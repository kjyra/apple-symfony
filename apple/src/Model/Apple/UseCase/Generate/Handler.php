<?php

namespace App\Model\Apple\UseCase\Generate;

use App\Model\Apple\Repository\AppleRepositoryInterface;
use App\Model\Flusher;
use App\Model\Apple\Entity\Apple\Apple;
use App\Model\Apple\Entity\Apple\Id;
use App\Model\Apple\Entity\Apple\Status;
use App\Model\Apple\Entity\Apple\Size;
use App\Model\Apple\Service\ColorGenerator;

/**
 * Description of Handler
 *
 * @author kjyra
 */
class Handler
{
	
	private $apples;
	private $flusher;
	private $colorGenerator;
	
	public function __construct(AppleRepositoryInterface $apples, Flusher $flusher, ColorGenerator $colorGenerator)
	{
		$this->apples = $apples;
		$this->flusher = $flusher;
		$this->colorGenerator = $colorGenerator;
	}
	
	public function handle()
	{
		
		$apples_count = random_int(3, 8);
		
		for($i = 0; $i < $apples_count; $i++) {
			$apple = new Apple(
					Id::next(),
					$this->colorGenerator->generate(),
					new \DateTimeImmutable(),
					new Status(Status::getOnTreeStatus()),
					new Size(1.00)
			);
			
			$this->apples->add($apple);
		}
		
		$this->flusher->flush();
	}
}
