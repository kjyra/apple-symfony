<?php

namespace App\Model\Apple\UseCase\Eat;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of Command
 *
 * @author kjyra
 */
class Command
{
	
	public $id;
	
	/**
	 * @var int 
	 * @Assert\Positive()
	 */
	public $percent;
	
	public function __construct(string $id)
	{
		$this->id = $id;
	}
}
