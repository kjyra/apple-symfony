<?php

namespace App\Model\Apple\UseCase\Eat;

use App\Model\Apple\Repository\AppleRepositoryInterface;
use App\Model\Flusher;
use App\Model\Apple\Entity\Apple\Id;

/**
 * Description of Handler
 *
 * @author kjyra
 */
class Handler
{
	
	private $apples;
	private $flusher;
	
	public function __construct(AppleRepositoryInterface $apples, Flusher $flusher)
	{
		$this->apples = $apples;
		$this->flusher = $flusher;
	}
	
	public function Handle(Command $command)
	{
		if(!$apple = $this->apples->getById(new Id($command->id))) {
			throw new DomainException('Apple not found');
		}		
		
		if($apple->eat($command->percent) == null) {
			$this->apples->delete($apple);
		}
		
		$this->flusher->flush();
	}
}
