<?php

namespace App\Model\Apple\UseCase\Delete\All;

use App\Model\Apple\Repository\AppleRepositoryInterface;
use App\Model\Flusher;

/**
 * Description of Handler
 *
 * @author kjyra
 */
class Handler
{
	
	private $apples;
	private $flusher;
	
	public function __construct(AppleRepositoryInterface $apples, Flusher $flusher)
	{
		$this->apples = $apples;
		$this->flusher = $flusher;
	}
	
	public function handle()
	{
		$apples = $this->apples->getAll();
		
		$this->apples->deleteAll($apples);
		
		$this->flusher->flush();
	}
}
