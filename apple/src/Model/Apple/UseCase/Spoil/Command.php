<?php

namespace App\Model\Apple\UseCase\Spoil;

/**
 * Description of Command
 *
 * @author kjyra
 */
class Command
{
	
	public $id;
	
	public function __construct(string $id)
	{
		$this->id = $id;
	}
}
