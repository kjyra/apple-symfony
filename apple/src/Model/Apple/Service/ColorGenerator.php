<?php

namespace App\Model\Apple\Service;

/**
 * Description of ColorGenerator
 *
 * @author kjyra
 */
class ColorGenerator
{
	
	public function generate()
	{
		$number = random_int(1, 5);
		
		$color = '';
		
		switch ($number) {
			case 1:
				$color = 'yellow';
				break;
			case 2:
				$color = 'green';
				break;
			case 3:
				$color = 'red';
				break;
			case 4:
				$color = 'golden';
				break;
			default:
				$color = 'rose';
				break;
		}
		
		return $color;
	}
}
