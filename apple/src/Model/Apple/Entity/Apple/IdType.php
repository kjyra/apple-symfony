<?php

namespace App\Model\Apple\Entity\Apple;

use Doctrine\DBAL\Types\GuidType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Description of IdType
 *
 * @author kjyra
 */
class IdType extends GuidType
{
    
    public const NAME = 'apple_apple_id';
    
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Id ? $value->getValue() : $value;
    }
    
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Id($value) : null;
    }
    
    public function getName(): string
    {
        return self::NAME;
    }
}
