<?php

namespace App\Model\Apple\Entity\Apple;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\FloatType;

/**
 * Description of EmailType
 *
 * @author kjyra
 */
class SizeType extends FloatType
{
    
    public const NAME = 'apple_apple_size';
    
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Size ? $value->getValue() : $value;
    }
    
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Size($value) : null;
    }
    
    public function getName(): string
    {
        return self::NAME;
    }
}
