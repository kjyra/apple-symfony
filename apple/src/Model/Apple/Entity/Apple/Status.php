<?php

namespace App\Model\Apple\Entity\Apple;

use Webmozart\Assert\Assert;

/**
 * Description of Status
 *
 * @author kjyra
 */
class Status
{	
	private const ON_TREE = 'apple is on tree';
	private const FELL = 'apple fell';
	private const SPOILED = 'apple is spoiled';	
	
	private $status;
	
	public function __construct(string $status)
	{
		Assert::oneOf($status, [
			self::ON_TREE,
			self::FELL,
			self::SPOILED,
		]);
		
		$this->status = $status;
	}
	
	public static function fall(): self
	{
		return new self(self::FELL);
	}
	
	public static function spoil(): self
	{
		return new self(self::SPOILED);
	}
	
	public function isOnTree(): bool
	{
		return $this->status === self::ON_TREE;
	}
	
	public function isFell(): bool
	{
		return $this->status === self::FELL;
	}
	
	public function getStatus()
	{
		return $this->status;
	}
	
	public static function getOnTreeStatus()
	{
		return self::ON_TREE;
	}
}
