<?php

namespace App\Model\Apple\Entity\Apple;

use Webmozart\Assert\Assert;
use InvalidArgumentException;

/**
 * Description of Size
 *
 * @author kjyra
 */
class Size
{
	
	/**
	 * @var decimal
	 */
	private $value;
	
	public function __construct(float $value)
	{
		Assert::notEmpty($value);
		if(!$this->checkValue($value) === 1) {
			throw new InvalidArgumentException('Incorrect number');
		}
		$this->value = $value;
	}
	
	public static function eat(int $percent, float $size)
	{
		$res = (float) $percent / 100;
		if($res >= $size) {
			return null;
		}
		$newSize = (float) $size - $res;
		return new self($newSize);
	}
	
	public function getInt()
	{
		return $this->value * 100;
	}
	
	public function checkValue($value)
	{
		$pattern = '#^[0-9].[0-9]{2}$#';
		$res = preg_match($pattern, $value);
		return $res;
	}
	
	public function getValue()
	{
		return $this->value;
	}
	
}
