<?php

namespace App\Model\Apple\Entity\Apple;

use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

/**
 * Description of Id
 *
 * @author kjyra
 */
class Id
{
    
    private $value;
    
    public function __construct(string $value)
    {
        Assert::notEmpty($value);
        $this->value = $value;
    }
    
    public static function next(): self
    {
        return new self(Uuid::uuid4()->toString());
    }
    
    public function getValue(): string
    {
        return $this->value;
    }
    
    public function __toString(): string
    {
        return $this->getValue();
    }
}
