<?php

namespace App\Model\Apple\Entity\Apple;

use Doctrine\ORM\Mapping as ORM;
use App\Model\StatusException;

/**
 * Description of Apple
 *
 * @author kjyra
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="apple_apples")
 */
class Apple
{
	
	/**
	 * @var Id
	 * @ORM\Column(type="apple_apple_id")
	 * @ORM\Id
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string")
	 * @var string 
	 */
	private $color;
	
	/**
	 * @var \DateTimeImmutable 
	 * @ORM\Column(type="datetime_immutable")
	 */
	private $date_of_birth;
	
	/**
	 * @var \DateTimeImmutable|null
	 * @ORM\Column(type="datetime_immutable", nullable=true)
	 */
	private $date_of_fall;
	
	/**
     * @var Status
     * @ORM\Column(type="apple_apple_status", length=20)
     */
	private $status;
	
	/**
	 * @var Size 
	 * @ORM\Column(type="apple_apple_size", precision=1, scale=2)
	 */
	private $size;
	
	public function __construct(Id $id, string $color, \DateTimeImmutable $date_of_birth, Status $status, Size $size)
	{
		$this->id = $id;
		$this->color = $color;
		$this->date_of_birth = $date_of_birth;
		$this->status = $status;
		$this->size = $size;
	}
	
	public function eat(int $percent)
	{
		if(!$this->status->isFell()) {
			throw new StatusException('Apple is spoiled or still on tree.');
		}
		$this->size = Size::eat($percent, $this->size->getValue());
		if($this->size === null) {
			return false;
		}
		return true;
	}
	
	public function fall()
	{
		if(!$this->status->isOnTree()) {
			throw new StatusException('Apple is spoiled or already fell.');
		}
		$this->date_of_fall = new \DateTimeImmutable();
		$this->status = Status::fall();
	}
	
	public function spoil()
	{
		if(!$this->status->isFell()) {
			throw new StatusException('Apple is on tree or already spoiled.');
		}
		$this->status = Status::spoil();
	}
	
	public function getIntSize()
	{
		return $this->size->getInt();
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getColor()
	{
		return $this->color;
	}
	
	public function getStatus()
	{
		return $this->status->getStatus();
	}
}
