<?php

namespace App\Model\Apple\Entity\Apple;

use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Description of EmailType
 *
 * @author kjyra
 */
class StatusType extends StringType
{
    
    public const NAME = 'apple_apple_status';
    
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Status ? $value->getStatus() : $value;
    }
    
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Status($value) : null;
    }
    
    public function getName(): string
    {
        return self::NAME;
    }
}
