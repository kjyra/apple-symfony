<?php

namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of Flusher
 *
 * @author kjyra
 */
class Flusher
{
	private $em;
	
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}
	
	public function flush(): void
	{
		$this->em->flush();
	}
}
