<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Service\User\Command;

/**
 * Description of Form
 *
 * @author kjyra
 */
class RegistrationFormType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', Type\EmailType::class)
            ->add('plainPassword', Type\RepeatedType::class, [
				'type' => Type\PasswordType::class,
				'first_options' => [
					'label' => 'Введите пароль'
				],
				'second_options' => [
					'label' => 'Повторите пароль'
				],
			]);
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}
