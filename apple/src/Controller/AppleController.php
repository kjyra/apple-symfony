<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\ReadModel\Apple\AppleFetcher;
use App\Model\Apple\Entity\Apple\Apple;
use App\Model\Apple\UseCase\Eat;
use App\Model\Apple\UseCase\Delete;
use App\Model\Apple\UseCase\Generate;
use App\Model\Apple\UseCase\Fall;
use App\Model\Apple\UseCase\Spoil;
use Psr\Log\LoggerInterface;


/**
 * Description of AppleController
 *
 * @author kjyra
 */
class AppleController extends AbstractController
{
	
	private $apples;
	private $logger;
	
	public function __construct(LoggerInterface $logger, AppleFetcher $apples)
	{
		$this->apples = $apples;
		$this->logger = $logger;
	}
	
	/**
	 * @Route("/apples", name="apple_index")
	 * @return Response
	 */
	public function index(): Response
	{
		$apples = $this->apples->findAll();
		return $this->render('app/apples/index.html.twig', [
			'apples' => $apples
		]);
	}
	
	/**
	 * @Route("/apple/{id}", name="apple_view")
	 * @param Apple $apple
	 * @param Request $request
	 * @param \App\Model\Apple\UseCase\Eat\Handler $handler
	 * @return Response
	 */
	public function view(Apple $apple, Request $request, Eat\Handler $handler): Response
	{
		$command = new Eat\Command($apple->getId());		
		$form = $this->createForm(Eat\Form::class, $command);
		$form->handleRequest($request);
		
		if($form->isSubmitted() && $form->isValid()) {
			try {
				$handler->handle($command);
                return $this->redirectToRoute('apple_index');
			} catch (\DomainException $ex) {
				$this->logger->error($ex->getMessage(), ['exception' => $ex]);
				$this->addFlash('error', $ex->getMessage());
				return $this->redirectToRoute('apple_index');
			}
		}
		return $this->render('app/apples/view.html.twig', [
			'apple' => $apple,
			'form' => $form->createView(),
		]);
	}
	
	/**
	 * @Route("/apple/delete/all", name="apple_delete_all")
	 * @param \App\Model\Apple\UseCase\Delete\All\Handler $handler
	 * @return Response
	 */
	public function deleteAll(Delete\All\Handler $handler): Response
	{
		try {
			$handler->handle();
            return $this->redirectToRoute('apple_index');
		} catch (\DomainException $ex) {
			$this->logger->error($ex->getMessage(), ['exception' => $ex]);
			$this->addFlash('error', $ex->getMessage());
			return $this->redirectToRoute('apple_index');
		}
	}
	
	/**
	 * @Route("/apple/delete/{id}", name="apple_delete")
	 * @param Apple $apple
	 * @param \App\Model\Apple\UseCase\Delete\One\Handler $handler
	 * @return Response
	 */
	public function delete(Apple $apple, Delete\One\Handler $handler): Response
	{
		$command = new Delete\One\Command($apple->getId());
		try {
			$handler->handle($command);
            return $this->redirectToRoute('apple_index');
		} catch (\DomainException $ex) {
			$this->logger->error($ex->getMessage(), ['exception' => $ex]);
			$this->addFlash('error', $ex->getMessage());
			return $this->redirectToRoute('apple_index');
		}
	}
	
	/**
	 * @Route("apples/generate", name="apple_generate")
	 * @param \App\Model\Apple\UseCase\Generate\Handler $handler
	 * @return Response
	 */
	public function generate(Generate\Handler $handler): Response
	{
		try {
			$handler->handle();
            return $this->redirectToRoute('apple_index');
		} catch (\DomainException $ex) {
			$this->logger->error($ex->getMessage(), ['exception' => $ex]);
			$this->addFlash('error', $ex->getMessage());
			return $this->redirectToRoute('apple_index');
		}
	}
	
	/**
	 * @Route("apple/fall/{id}", name="apple_fall")
	 * @param Apple $apple
	 * @param \App\Model\Apple\UseCase\Fall\Handler $handler
	 * @return Response
	 */
	public function fall(Apple $apple, Fall\Handler $handler): Response
	{
		$command = new Fall\Command($apple->getId());
		try {
			$handler->handle($command);
            return $this->redirectToRoute('apple_index');
		} catch (\DomainException $ex) {
			$this->logger->error($ex->getMessage(), ['exception' => $ex]);
			$this->addFlash('error', $ex->getMessage());
			return $this->redirectToRoute('apple_index');
		}
	}
	
	/**
	 * @Route("apple/spoil/{id}", name="apple_spoil")
	 * @param Apple $apple
	 * @param \App\Model\Apple\UseCase\Spoil\Handler $handler
	 * @return Response
	 */
	public function spoil(Apple $apple, Spoil\Handler $handler): Response
	{
		$command = new Spoil\Command($apple->getId());
		try {
			$handler->handle($command);
            return $this->redirectToRoute('apple_index');
		} catch (\DomainException $ex) {
			$this->logger->error($ex->getMessage(), ['exception' => $ex]);
			$this->addFlash('error', $ex->getMessage());
			return $this->redirectToRoute('apple_index');
		}
	}
}
