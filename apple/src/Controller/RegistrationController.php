<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\User\UserService;
use App\Form\RegistrationFormType;
use App\Service\User\Command;

/**
 * Description of RegistrationController
 *
 * @author kjyra
 */
class RegistrationController extends AbstractController
{
	
	/**
	 * @Route("/register", name="user_register")
	 * @param Request $request
	 * @param UserService $handler
	 * @return Response
	 */
	public function register(Request $request, UserService $handler): Response
	{
		$command = new Command();
		$form = $this->createForm(RegistrationFormType::class, $command);
		$form->handleRequest($request);
		
		if($form->isSubmitted() && $form->isValid()) {
			$handler->handleAdmin($command);
            $this->addFlash('success', 'Пользователь успешно зарегистрирован!');
			return $this->redirectToRoute('app_login');
		}
		
		return $this->render('security/register.html.twig', [
			'form' => $form->createView()
		]);
	}
	
}
