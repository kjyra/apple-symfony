<?php

namespace App\ReadModel\Apple;

/**
 * Description of AppleView
 *
 * @author kjyra
 */
class AppleView
{
	
	public $id;
	public $size;
	public $status;
	public $color;
	
	public function getIntSize()
	{
		return $this->size * 100;
	}
}
