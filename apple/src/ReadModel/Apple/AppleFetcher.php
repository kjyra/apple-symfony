<?php

namespace App\ReadModel\Apple;

use Doctrine\DBAL\Connection;

/**
 * Description of AppleFetcher
 *
 * @author kjyra
 */
class AppleFetcher
{
	
	private $connection;
	
	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}
	
	public function findAll(): ?array
	{
		$stmt = $this->connection->createQueryBuilder()
                ->select('id', 'size', 'status', 'color')
                ->from('apple_apples')
                ->execute();
		
		$stmt->setFetchMode(\Doctrine\DBAL\FetchMode::CUSTOM_OBJECT, AppleView::class);
		$result = $stmt->fetchAll();
		
		return $result ?: null;
	}
	
}
