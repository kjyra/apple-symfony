<?php

namespace App\Service\User;

/**
 * Description of Command
 *
 * @author kjyra
 */
class Command
{
	
	/**
	 * @var string
	 */
	public $email;
	
	/**
	 * @var string
	 */
	public $plainPassword;
}
