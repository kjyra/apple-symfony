<?php

namespace App\Service\User;

use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Description of UserService
 *
 * @author kjyra
 */
class UserService
{
	
	private $users;
	private $passwordEncoder;
	
	public function __construct(UserRepository $users, UserPasswordEncoderInterface $passwordEncoder)
	{
		$this->users = $users;
		$this->passwordEncoder = $passwordEncoder;
	}
	
	public function handleAdmin(Command $command)
	{		
		$user = new User();
		$user->setEmail($command->email);
		$user->setPassword($this->passwordEncoder->encodePassword($user, $command->plainPassword));
		$user->setRoles(["ROLE_ADMIN"]);
		$this->users->createUser($user);
	}
	
	public function handleUser(Command $command)
	{		
		$user = new User();
		$user->setEmail($command->email);
		$user->setPassword($this->passwordEncoder->encodePassword($user, $command->plainPassword));
		$this->users->createUser($user);
	}
}
